package uz.egov.id.example

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import uz.egov.id.example.databinding.FragmentStartBinding

class StartFragment : Fragment() {

    private lateinit var content: FragmentStartBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        content = FragmentStartBinding.inflate(inflater)
        return content.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        content.buttonAuth.setOnClickListener {

            try {
                findNavController().navigate(
                    R.id.action_fragment_start_to_fragment_auth
                )
            } catch (e: Exception) {

            }


        }

        content.buttonReg.setOnClickListener {

            try {
                findNavController().navigate(
                    R.id.action_fragment_start_to_fragment_reg
                )
            } catch (e: Exception) {

            }

        }

    }


}