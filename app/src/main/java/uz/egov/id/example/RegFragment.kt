package uz.egov.id.example

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import uz.egov.id.sdk.OneIDSDK
import uz.egov.id.sdk.Status
import uz.egov.id.example.databinding.FragmentRegisterBinding

class RegFragment : Fragment() {

    private lateinit var content: FragmentRegisterBinding
    private var disposable: Disposable? = null

    private var regToken = ""
    private var codeToken = ""

    val scannerLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {

                val data: Intent? = result.data

                val docNumber = data?.getStringExtra("docNumber") ?: ""
                val birthDate = data?.getStringExtra("birthDate")

                content.editTextSerial.setText(docNumber)
                content.editTextBirthday.setText(birthDate)

            }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        content = FragmentRegisterBinding.inflate(inflater)
        return content.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        content.layoutSerial.setEndIconOnClickListener {
            OneIDSDK.scanDoc(it.context, scannerLauncher)
        }

        content.buttonSendCode.setOnClickListener {

            val serial = content.editTextSerial.text.toString()
            val birthday = content.editTextBirthday.text.toString()
            val email = content.editTextEmail.text.toString()
            val phone = content.editTextPhone.text.toString()

            when {

                serial.isNotEmpty() && birthday.isNotEmpty() && email.isNotEmpty() && phone.isNotEmpty() -> {
                    disposable?.dispose()
                    disposable = OneIDSDK.startReg(
                        serial = serial,
                        birthday = birthday,
                        email = email,
                        phone = phone
                    )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            when (it) {

                                Status.INVALID_LOGIN_PASS -> {
                                    Snackbar.make(
                                        view,
                                        "Invalid login or password",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                }
                                Status.SERVER_ERROR -> {
                                    Snackbar.make(
                                        view,
                                        "Server error",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                }
                                is Status.OK -> {
                                    regToken = it.token
                                }
                            }

                        }, {

                        })
                }
            }

        }

        content.buttonCheckCode.setOnClickListener {

            val code = content.editTextCode.text.toString()

            when {

                code.isNotEmpty() -> {
                    disposable?.dispose()
                    disposable = OneIDSDK.checkCode(regToken, code)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            when (it) {
                                Status.INVALID_LOGIN_PASS -> {
                                    Snackbar.make(
                                        view,
                                        "Invalid login or password",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                }
                                Status.SERVER_ERROR -> {
                                    Snackbar.make(
                                        view,
                                        "Server error",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                }
                                is Status.OK -> {
                                    codeToken = it.token
                                }
                            }

                        }, {

                        })
                }
            }

        }

        content.buttonReg.setOnClickListener {

            val login = content.editTextLogin.text.toString()
            val password = content.editTextPassword.text.toString()

            when {

                login.isNotEmpty() && password.isNotEmpty() -> {
                    disposable?.dispose()
                    disposable = OneIDSDK.register(regToken, login, password)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ s ->
                            when (s) {
                                Status.INVALID_LOGIN_PASS -> {
                                    Snackbar.make(
                                        view,
                                        "Invalid login or password",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                }
                                Status.SERVER_ERROR -> {
                                    Snackbar.make(
                                        view,
                                        "Server error",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                }
                                is Status.OK -> {
                                    findNavController().popBackStack()
                                }
                            }

                        }, {

                        })
                }
            }

        }

    }


}