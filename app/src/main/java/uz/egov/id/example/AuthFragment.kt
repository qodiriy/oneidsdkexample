package uz.egov.id.example

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import uz.egov.id.sdk.OneIDSDK
import uz.egov.id.sdk.Status
import uz.egov.id.example.databinding.FragmentAuthBinding

class AuthFragment : Fragment() {


    private lateinit var content: FragmentAuthBinding
    private var disposable: Disposable? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        content = FragmentAuthBinding.inflate(inflater)
        return content.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        content.buttonAuth.setOnClickListener {

            val login = content.editTextLogin.text.toString()
            val password = content.editTextPassword.text.toString()

            when {

                login.isNotEmpty() && password.isNotEmpty() -> {
                    disposable?.dispose()
                    disposable =
                        OneIDSDK.startAuth("1", "gN0C1dVA76NcH8FpJW81t2Sb", login, password)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                Log.e("Status", it.first.toString())
                                when (it.first) {
                                    Status.INVALID_LOGIN_PASS -> {
                                        Snackbar.make(
                                            view,
                                            "Invalid login or password",
                                            Snackbar.LENGTH_SHORT
                                        ).show()
                                    }
                                    Status.SERVER_ERROR -> {
                                        Snackbar.make(
                                            view,
                                            "Server error",
                                            Snackbar.LENGTH_SHORT
                                        ).show()
                                    }
                                    is Status.OK -> {
                                        val bundle = bundleOf(Pair("user", it.second))
                                        findNavController().navigate(
                                            R.id.action_fragment_auth_to_fragment_info,
                                            bundle
                                        )
                                    }
                                }

                            }, {

                            })
                }
            }

        }

    }


}