package uz.egov.id.example

import android.app.Application
import uz.egov.id.sdk.OneIDSDK

class ExampleApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        OneIDSDK.init()
    }

}